﻿using System;

namespace ReverseString
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public static void ReverseString(char[] s) {
            int i = 0;
            int j = s.Length - 1;
            
            while(i < j){
                Swap(s, i, j);
                i++;
                j--;
            }
        }
        
        public static void Swap(char[] v, int a, int b){
            char temp = v[a];
            v[a] = v[b];
            v[b] = temp;
        }
    }
}